package project.app.common;

import java.util.Scanner;

import project.app.board.AnonBoard;
import project.app.member.Member;
import project.app.member.MembersDAO;




public class LoginControl {
	//출력클래스
	
	private Scanner sc = new Scanner(System.in);
	private static Member loginInfo = null;
	public static Member getLoginInfo() {
		return loginInfo;
	}
		MembersDAO membersDAO = MembersDAO.getInstance();
	
		public LoginControl() {
		while(true) {
			menuPrint();			
			int menuNo = menuSelect();			
			if(menuNo == 1) {
				//회원가입
				join();
			}else if(menuNo == 2) {
				//로그인
				login();
			}else if(menuNo == 3) {
				//게시판
				new AnonBoard().run();
			}else if(menuNo == 4) {
				//종료
				exit();
				break;
			}else {
				showInputError();
			}
		}
	}
	
	private void menuPrint() {
		System.out.println("===============================");
		System.out.println("1.회원가입 2.로그인 3.익명게시판 2.종료");
		System.out.println("===============================");
	}
	
	private int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("숫자 형식으로 입력해주세요.");
		}
		return menuNo;
	}
	
	private void exit() {
		System.out.println("프로그램을 종료합니다.");
	}
	
	private void showInputError() {
		System.out.println("메뉴를 확인해주시기 바랍니다.");
	}
	
	private void login() {
		//아이디와 비밀번호 입력
		Member inputInfo = inputMember();
		//로그인 시도
		loginInfo = membersDAO.selectOne(inputInfo);
		
		//실패할 경우 그대로 메소드 종료
		if(loginInfo == null) return;
		
		//성공할 경우 프로그램을 실행
		System.out.println("로그인 성공!");
		new BoardManagement().run();
	}
	
	private Member inputMember() {
		Member info = new Member();
		System.out.print("아이디 > ");
		info.setMem_id(sc.nextLine());
		System.out.print("비밀번호 > ");
		info.setPw(sc.nextLine());
		
		return info;
	}
	private void join(){
		
		Member member = insertAll();
		membersDAO.insert(member);
		
		
		
	}	
	
	private  Member insertAll() {
			Member member = new Member();
			System.out.println("회원코드>");
			member.setMem_co(Integer.parseInt(sc.nextLine()));
			System.out.println("아이디>");
			member.setMem_id(sc.nextLine());
			System.out.println("닉네임>");
			member.setMem_nick(sc.nextLine());
			System.out.println("비밀번호>");
			member.setPw(sc.nextLine());
			System.out.println("이름>");
			member.setName(sc.nextLine());
			System.out.println("생년월일(yyyy-MM-dd)>");
			member.setBirth(Integer.parseInt(sc.nextLine()));
			System.out.println("성별> ");
			member.setGender(sc.nextLine());
			System.out.println("이메일> ");
			member.setEmail(sc.nextLine());
			System.out.println("핸드폰번호> ");
			member.setPhone(Integer.parseInt(sc.nextLine()));
			
			return member;
		}
}
