package project.app.common;

import java.util.Scanner;

import project.app.board.AnonBoard;
import project.app.board.FreeBoard;
import project.app.board.NoticeBoard;

public class BoardManagement {

	private Scanner sc = new Scanner(System.in);
	//private static Member loginInfo = null;
	//public static Member getLoginInfo() {
	//	return loginInfo;
	//}
	
	
	public void run() {
		while(true) {
			menuPrint();			
			int menuNo = menuSelect();			
			if(menuNo == 1) {
				//공지사항
				new NoticeBoard().run();
			}else if(menuNo == 2) {
				//자유게시판
				new FreeBoard();
			}else if(menuNo == 3) {
				//익명게시판
				new AnonBoard();
			}else if(menuNo == 4) {
				//종료
				exit();
				break;
			}else {
				showInputError();
			}
		}
	}
	
	private void menuPrint() {
		System.out.println("=====================================");
		System.out.println("1.공지사항 2.자유게시판 3.익명게시판 4.뒤로가기");
		System.out.println("=====================================");
	}
	
	private int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("숫자 형식으로 입력해주세요.");
		}
		return menuNo;
	}
	
	private void exit() {
		System.out.println("이전으로 돌아갑니다.");
	}
	
	private void showInputError() {
		System.out.println("메뉴를 확인해주시기 바랍니다.");
	}

	
	
}
