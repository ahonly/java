package project.app.member;

import java.sql.SQLException;

import project.app.common.DAO;


public class MembersDAO extends DAO{

	
	//싱글톤
		private static MembersDAO dao = null;
		private MembersDAO() {}
		public static MembersDAO getInstance() {
			if(dao == null) {
				dao = new MembersDAO();
			}
			return dao;
		}
	
		//CRUD
		public Member selectOne(Member member) {
			Member loginInfo = null;
			try {
				connect();
				String sql = "SELECT * FROM Member WHERE mem_id = '" + member.getMem_id() +"'"; 
				stmt = conn.createStatement();
				rs = stmt.executeQuery(sql);
				
				if(rs.next()) {
					//아이디 존재
					if(rs.getString("pw").equals(member.getPw())){
						//비밀번호 일치
						// -> 로그인 성공
						loginInfo = new Member();
						loginInfo.setMem_id(rs.getString("mem_id"));
						loginInfo.setPw(rs.getString("pw"));
						loginInfo.setMem_co(rs.getInt("mem_co"));
						loginInfo.setMem_nick(rs.getString("mem_nick"));
					}else {
						System.out.println("비밀번호가 일치하지 않습니다.");
					}
				}else {
					System.out.println("아이디가 존재하지 않습니다.");
				}
				
			}catch(SQLException e) {
				e.printStackTrace();
			}finally {
				disconnect();
			}	
			return loginInfo;
		}
		//회원가입
		public void insert(Member member) {
			try {
				connect();
				String sql =  "INSERT INTO Member VALUES (?,?,?,?,?,?,?,?,?)";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, member.getMem_co());
				pstmt.setString(2, member.getMem_id());
				pstmt.setString(3, member.getMem_nick());
				pstmt.setString(4, member.getPw());
				pstmt.setString(5, member.getName());
				pstmt.setInt(6, member.getBirth());
				pstmt.setString(7, member.getGender());
				pstmt.setString(8, member.getEmail());
				pstmt.setInt(9, member.getPhone());
				
				int result = pstmt.executeUpdate();
				
				if(result > 0) {
					System.out.println("회원가입이 완료되었습니다.");
				}else {
					System.out.println("이미 정보가 있습니다.");
				}
				
			}catch(SQLException e) {
				e.printStackTrace();
			}finally {
				disconnect();
			}
		}
		
}
