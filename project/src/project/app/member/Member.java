package project.app.member;

public class Member {

	
	//회원코드//관리자 1 유저 2
	private int mem_co;
	

	//아이디 (중복불가) 
	private String mem_id;
	//닉네임 (중복불가) 
	private String mem_nick;
	//비밀번호 
	private String pw;
	//이름 
	private String name;
	//생년월일 (yyyy-MM-dd) 
	private int birth;
	//성별 
	private String gender;
	//이메일 (중복불가) 
	private String email;
	//핸드폰번호 (중복불가) 
	private int phone;


	public int getMem_co() {
		return mem_co;
	}

	public String getMem_id() {
		return mem_id;
	}

	public String getMem_nick() {
		return mem_nick;
	}


	public String getPw() {
		return pw;
	}

	public String getName() {
		return name;
	}

	public int getBirth() {
		return birth;
	}

	public String getGender() {
		return gender;
	}

	public String getEmail() {
		return email;
	}

	public int getPhone() {
		return phone;
	}

	public void setMem_co(int mem_co) {
		this.mem_co = mem_co;
	}

	public void setMem_id(String mem_id) {
		this.mem_id = mem_id;
	}

	public void setMem_nick(String mem_nick) {
		this.mem_nick = mem_nick;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBirth(int birth) {
		this.birth = birth;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		String info = "";
		
		if(mem_co == 1) {
			info = "관리자 계정 : " + mem_id;
		}else if(mem_co == 2){
			info = "일반 계정 : " + mem_id;
		}
	
		return info;
	}
	
}
