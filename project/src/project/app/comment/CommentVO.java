package project.app.comment;

public class CommentVO {

	//글번호 
	private int co_po_no; //(==	int po_no;)
	//댓글번호 
	private int co_no;
	//닉네임 
	private String co_nic;
	//아이디 
	private String co_id;
	//내용 
	private String co_con;
	
	
	//익명
	private String co_noId;
	//익명비밀번호 
	private String co_pw;
	
	
	public int getCo_po_no() {
		return co_po_no;
	}
	public int getCo_no() {
		return co_no;
	}
	public String getCo_nic() {
		return co_nic;
	}
	public String getCo_id() {
		return co_id;
	}
	public String getCo_con() {
		return co_con;
	}
	public String getCo_noId() {
		return co_noId;
	}
	public String getCo_pw() {
		return co_pw;
	}
	public void setCo_po_no(int co_po_no) {
		this.co_po_no = co_po_no;
	}
	public void setCo_no(int co_no) {
		this.co_no = co_no;
	}
	public void setCo_nic(String co_nic) {
		this.co_nic = co_nic;
	}
	public void setCo_id(String co_id) {
		this.co_id = co_id;
	}
	public void setCo_con(String co_con) {
		this.co_con = co_con;
	}
	public void setCo_noId(String co_noId) {
		this.co_noId = co_noId;
	}
	public void setCo_pw(String co_pw) {
		this.co_pw = co_pw;
	}
	
	
	
}

