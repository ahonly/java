package project.app.comment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import project.app.common.DAO;

public class CommentDAOImpl extends DAO implements CommentDAO{

	// 싱글톤
		private static CommentDAO instance = null;

		public static CommentDAO getInstance() {
			if (instance == null)
				instance = new CommentDAOImpl();
			return instance;
		}

		@Override
		//게시글의 댓글 모두 보기
		public List<CommentVO> selectAll(int poNo){
			List<CommentVO> list = new ArrayList<>();
			try {
				connect();
				String sql = "SELECT * FROM Comment WHERE co_po_no = " + poNo;
				stmt = conn.createStatement();
				rs = stmt.executeQuery(sql);
				
				while(rs.next()) {
					CommentVO commentVO = new CommentVO();
					commentVO.setCo_po_no(rs.getInt("co_po_no"));
					commentVO.setCo_no(rs.getInt("co_no"));
					commentVO.setCo_id(rs.getString("co_id"));
					commentVO.setCo_nic(rs.getString("co_nic"));
					commentVO.setCo_con(rs.getString("co_con"));
					list.add(commentVO);
				}
				
			}catch(SQLException e) {
				e.printStackTrace();
			}finally {
				disconnect();
			}
			return list;
		}


		@Override // 글쓰기
		public void insert(CommentVO commentVO) {
			try {
				connect();
				String sql = "INSERT INTO Comment VALUES (?,?,?,?,?,?,?)";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, commentVO.getCo_po_no());
				pstmt.setInt(2, commentVO.getCo_no());
				pstmt.setString(3, commentVO.getCo_id());
				pstmt.setString(4, commentVO.getCo_nic());
				pstmt.setString(5, commentVO.getCo_con());
				pstmt.setString(6, commentVO.getCo_noId());
				pstmt.setString(7, commentVO.getCo_pw());

				int result = pstmt.executeUpdate();
				if (result > 0) {
					System.out.println("정상적으로 등록되었습니다.");

				} else {
					System.out.println("정상적으로 등록되지 않았습니다.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				disconnect();
			}

		}

	//댓글수정?????????????????????????????????????????????????????
		@Override
		public void update(CommentVO commentVO) {
			try {
				connect();
				String sql = "UPDATE Comment SET co_con = ? WHERE po_no = "+commentVO.getCo_no();
				System.out.println(sql);
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, commentVO.getCo_con());

				int result = pstmt.executeUpdate();
				if (result > 0) {
					System.out.println("정상적으로 등록되었습니다.");

				} else {
					System.out.println("정상적으로 등록되지 않았습니다.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				disconnect();
			}
		}

		@Override
		public void delete(CommentVO commentVO) {
			try {
				connect();
				String sql = "DELETE FROM Comment WHERE co_no =" + commentVO.getCo_no() +" AND co_po_no =" + commentVO.getCo_po_no();
				int result = stmt.executeUpdate(sql);

				if (result > 0) {
					System.out.println("정상적으로 삭제되었습니다.");

				} else {
					System.out.println("정상적으로 삭제되지 않았습니다.");
				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				disconnect();
			}

		}

	
			
		}

	