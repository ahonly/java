package project.app.comment;

import java.util.List;
import java.util.Scanner;

import project.app.board.BoardDAO;
import project.app.board.BoardDAOImpl;
import project.app.board.BoardVO;
import project.app.common.LoginControl;
import project.app.member.MembersDAO;



public class CommentManagement {

	
	//필드
		protected Scanner sc = new Scanner(System.in);
		protected BoardDAO bDAO = BoardDAOImpl.getInstance();
		protected MembersDAO rDAO = MembersDAO.getInstance();
		protected CommentDAO cDAO = CommentDAOImpl.getInstance();
		
		//생성자 -> run
		public void run() {
			//댓글입력/댓글수정/댓글삭제
			//목록보여주고 all//목록에 넣어줄 것 글번호 아이디 글쓴이닉네임 제목 출력
			//글번호 입력하게하기=> 글보여주기 selectOne// 제목 닉네임,아이디 내용+ 댓글목록출력(입력한글번호의 댓글) 댓글넘버 닉네임 아이디 내용 출력
			//댓글상세/글수정/글삭제/목록 돌아가기
			while(true) {
				menuPrint();			
				int menuNo = menuSelect();			
				if(menuNo == 1) {
					//댓글입력
					inputComment();
				}else if(menuNo == 2) {
					//댓글수정
					updateComment();
				}else if(menuNo == 3) {
					//댓글삭제
					deleteComment();
				}else if(menuNo == 4) {
					//종료
					exit();
					break;
				}else {
					showInputError();
				}
			}
		}
		//댓글삭제/포스트넘버+댓글넘버
		private void deleteComment() {
			
			
		}
		//댓글수정
		private void updateComment() {
			
			
		}
		
		//댓글쓰기/포스트넘버 댓글내용
		private void inputComment() {
			
			
		}
		//메소드
		protected void menuPrint() {
			System.out.println("=================================");
			System.out.println("1.댓글입력 2.댓글수정 3.댓글삭제 2.뒤로가기");
			System.out.println("=================================");
		}
		
		protected int menuSelect() {
			int menuNo = 0;
			try {
				menuNo = Integer.parseInt(sc.nextLine());
			}catch(NumberFormatException e) {
				System.out.println("숫자를 입력해주시기 바랍니다.");
			}
			return menuNo;
		}
		
		protected void exit() {
			System.out.println("프로그램을 종료합니다.");
		}
		
		protected void showInputError() {
			System.out.println("메뉴에서 입력해주시기 바랍니다.");
		}
		
		protected boolean selectRole() {
			int memberRole = LoginControl.getLoginInfo().getMem_co();
			if(memberRole == 0) {
				return true;
			}else {
				return false;
			}
		}

	}


