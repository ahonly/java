package project.app.comment;

import java.util.List;

import project.app.board.BoardVO;

public interface CommentDAO {

	
	//댓글입력/댓글수정/댓글삭제
	
	// 전체조회
	List<CommentVO> selectAll(int poNo);


	// 등록
	void insert(CommentVO commentVO);

	// 수정
	void update(CommentVO commentVO);

	// 삭제
	void delete(CommentVO commentVO);


}
