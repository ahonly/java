package project.app.board;

import java.util.List;

public interface BoardDAO {

	// 전체조회
	List<BoardVO> selectAll(int poType);
	List<BoardVO> selectAll();

	// 단건조회
	BoardVO selectOne(int boardNO);

	// 등록
	void insert(BoardVO boardVO);

	// 삭제
	void delete(int boardNo);

	// 글수정
	void updatePost(BoardVO boardVO);

	// 글삭제(게시판제한)
	void delete(BoardVO boardVO);
	
	//익명게시판용글삭
	void anonDelete(BoardVO boardVO);

}
