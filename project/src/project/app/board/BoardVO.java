package project.app.board;

public class BoardVO {

	//글타입 공지사항1/자유게시판2/익명게시판3
	int po_type;
	//글번호 
	int po_no;
	//아이디 
	private String po_id;
	//닉네임 
	private String po_nic;
	//제목 
	private String po_tit;
	//내용 
	private String po_con;
	
	
	//익명
	private String po_noId;
	//익명비밀번호
	private String po_pw;
	
	
	
	//get set
	public int getPo_type() {
		return po_type;
	}
	public int getPo_no() {
		return po_no;
	}
	public String getPo_id() {
		return po_id;
	}
	public String getPo_nic() {
		return po_nic;
	}
	public String getPo_tit() {
		return po_tit;
	}
	public String getPo_con() {
		return po_con;
	}
	public String getPo_noId() {
		return po_noId;
	}
	public String getPo_pw() {
		return po_pw;
	}
	public void setPo_type(int po_type) {
		this.po_type = po_type;
	}
	public void setPo_no(int po_no) {
		this.po_no = po_no;
	}
	public void setPo_id(String po_id) {
		this.po_id = po_id;
	}
	public void setPo_nic(String po_nic) {
		this.po_nic = po_nic;
	}
	public void setPo_tit(String po_tit) {
		this.po_tit = po_tit;
	}
	public void setPo_con(String po_con) {
		this.po_con = po_con;
	}
	public void setPo_noId(String po_noId) {
		this.po_noId = po_noId;
	}
	public void setPo_pw(String po_pw) {
		this.po_pw = po_pw;
	}

}
