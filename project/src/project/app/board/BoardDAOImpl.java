package project.app.board;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import project.app.common.DAO;

public class BoardDAOImpl extends DAO implements BoardDAO {

	// 싱글톤
	private static BoardDAO instance = null;

	public static BoardDAO getInstance() {
		if (instance == null)
			instance = new BoardDAOImpl();
		return instance;
	}


	//게시판별 글 목록보기
	public List<BoardVO> selectAll(int poType){
		List<BoardVO> list = new ArrayList<>();
		try {
			connect();
			String sql = "SELECT * FROM Board WHERE po_type = " + poType;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				BoardVO boardVO = new BoardVO();
				boardVO.setPo_type(rs.getInt("po_type"));
				boardVO.setPo_no(rs.getInt("po_no"));
				boardVO.setPo_id(rs.getString("po_id"));
				boardVO.setPo_nic(rs.getString("po_nic"));		
				boardVO.setPo_tit(rs.getString("po_tit"));
				boardVO.setPo_con(rs.getString("po_con"));
				list.add(boardVO);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		return list;
	}
	public List<BoardVO> selectAll(){
		List<BoardVO> list = new ArrayList<>();
		try {
			connect();
			String sql = "SELECT * FROM Board  ";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				BoardVO boardVO = new BoardVO();
				boardVO.setPo_type(rs.getInt("po_type"));
				boardVO.setPo_no(rs.getInt("po_no"));
				boardVO.setPo_id(rs.getString("po_id"));
				boardVO.setPo_nic(rs.getString("po_nic"));		
				boardVO.setPo_tit(rs.getString("po_tit"));
				boardVO.setPo_con(rs.getString("po_con"));
				list.add(boardVO);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		return list;
	}
	//글검색
	public BoardVO selectOne(int boardNO) {
		BoardVO boardVO = null;
		try {
			connect();
			String sql = "SELECT * FROM Board WHERE po_no = ?";
		pstmt = conn.prepareStatement(sql);
		pstmt.setInt(1, boardNO);
		
		rs = pstmt.executeQuery();
		
		if(rs.next()) {
			boardVO = new BoardVO();
			//입력숫자해당 글보기 /제목 닉네임,아이디 내용 출력
			boardVO.setPo_type(rs.getInt("po_type"));
			boardVO.setPo_no(rs.getInt("po_no"));
			boardVO.setPo_tit(rs.getString("po_tit"));
			boardVO.setPo_nic(rs.getString("po_nic"));
			boardVO.setPo_id(rs.getString("po_id"));
			boardVO.setPo_con(rs.getString("po_con"));	
			}	
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
		return boardVO;
	}
	

	@Override // 글쓰기
	public void insert(BoardVO boardVO) {
	
		try {
			connect();
			String sql = "INSERT INTO Board VALUES (?,?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, boardVO.getPo_type());
			pstmt.setInt(2, boardVO.getPo_no());
			pstmt.setString(3, boardVO.getPo_id());
			pstmt.setString(4, boardVO.getPo_nic());
			pstmt.setString(5, boardVO.getPo_tit());
			pstmt.setString(6, boardVO.getPo_con());
			pstmt.setString(7, boardVO.getPo_noId());
			pstmt.setString(8, boardVO.getPo_pw());
			
			//boardVO.setPo_nic(rs.getString("po_nic"));
			//boardVO.setPo_id(rs.getString("po_id"));
			//boardVO.setPo_id(rs.getString("po_pw"));

			int result = pstmt.executeUpdate();
			if (result > 0) {
				System.out.println("정상적으로 등록되었습니다.");

			} else {
				System.out.println("정상적으로 등록되지 않았습니다.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}

	}

	@Override//글수정?????????????????????????????????????????????????????
	public void updatePost(BoardVO boardVO) {
		try {
			connect();
			String sql = "UPDATE Board SET po_tit = ?, po_con = ? WHERE po_no = "+boardVO.getPo_no();
			System.out.println(sql);
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, boardVO.getPo_tit());
			pstmt.setString(2, boardVO.getPo_con());

		
			
			int result = pstmt.executeUpdate();
			
			if(result > 0) {
				System.out.println("정상적으로 수정되었습니다.");
			}else {
				System.out.println("정상적으로 수정되지 않았습니다.");
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
	}

	//글삭제
	@Override
	public void delete(BoardVO boardVO) {
		try {
			connect();
			String sql = "DELETE FROM Board WHERE po_no ="+ boardVO.getPo_no() +" AND po_type =" + boardVO.getPo_type();
			int result = stmt.executeUpdate(sql);

			if (result > 0) {
				System.out.println("정상적으로 삭제되었습니다.");

			} else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}

	}


	@Override
	public void delete(int boardNo) {
		// TODO Auto-generated method stub
		
	}
	//익명게시판용글삭제
	public void anonDelete(BoardVO boardVO) {
		try {
			connect();
			String sql = "DELETE FROM Board WHERE po_no ="+ boardVO.getPo_no() +" AND po_type =" + boardVO.getPo_type() +
												"AND po_noId = " + boardVO.getPo_noId() + "AND po_pw = " + boardVO.getPo_pw();
			int result = stmt.executeUpdate(sql);

			if (result > 0) {
				System.out.println("정상적으로 삭제되었습니다.");

			} else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}

	}













	
}
