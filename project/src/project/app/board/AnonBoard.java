package project.app.board;

import java.util.List;
import java.util.Scanner;

import project.app.comment.CommentDAO;
import project.app.comment.CommentDAOImpl;
import project.app.comment.CommentManagement;
import project.app.comment.CommentVO;
import project.app.common.LoginControl;
import project.app.member.MembersDAO;

public class AnonBoard {

	// 필드
	protected Scanner sc = new Scanner(System.in);
	protected BoardDAO bDAO = BoardDAOImpl.getInstance();
	protected MembersDAO rDAO = MembersDAO.getInstance();
	protected CommentDAO cDAO = CommentDAOImpl.getInstance();
	int boardType = 3;
	

	// 익명게시판아이디비밀번호
	private static BoardVO AnonPostId = null;

	public static BoardVO getAnonId() {
		return AnonPostId;
	}

	// 생성자 -> run
	public void run() {

		// 목록보여주고 all//목록에 넣어줄 것 글번호 아이디 글쓴이닉네임 제목 출력
		// 글번호 입력하게하기=> 글보여주기 selectOne// 제목 닉네임,아이디 내용+ 댓글목록출력(입력한글번호의 댓글) 댓글넘버 닉네임 아이디
		// 내용 출력
		// 댓글상세/글수정/글삭제/목록 돌아가기
		while (true) {
			System.out.print("**익명게시판**");
			selectAll();

			menuPrint();

			int menuNo = intScan();

			if (menuNo == 1) {
				// 글보기
				viewPost();
			} else if (menuNo == 2) {
				// 글쓰기
				writePost();
			} else if (menuNo == 3) {
				// 글삭제
				delete();
			} else if (menuNo == 4) {
				// 프로그램 종료
				exit();
				break;
			} else {
				// 입력오류
				showInputError();
			}
		}
	}

	// 메소드
	protected void menuPrint() {
		System.out.println("==============================");
		System.out.println("1.글보기 2.글쓰기 3.글삭제 4.뒤로가기");
		System.out.println("==============================");
	}

	// 스캐너
	protected int intScan() {
		int intScann = 0;
		try {
			intScann = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("숫자를 입력해주시기 바랍니다.");
		}
		return intScann;
	}

	protected void exit() {
		System.out.println("이전 화면으로 돌아갑니다.");
	}

	protected void showInputError() {
		System.out.println("메뉴에서 입력해주시기 바랍니다.");
	}

	protected boolean selectRole() {
		int memberRole = LoginControl.getLoginInfo().getMem_co();
		if (memberRole == 0) {
			return true;
		} else {
			return false;
		}
	}

	// 글쓰기
	private void writePost() {
		BoardVO anonPostInfo = new BoardVO();

		// 익명게시판 글쓰기 아이디 비밀번호
		System.out.print("익명아이디 입력>");
		String po_noId = sc.nextLine();
		System.out.print("익명비밀번호 입력>");
		String po_pw = sc.nextLine();
		anonPostInfo.setPo_noId(po_noId);
		anonPostInfo.setPo_pw(po_pw);

		AnonPostId = anonPostInfo;

		BoardVO boardVO = new BoardVO();
		System.out.print("제목입력>");
		String title = sc.nextLine();

		System.out.println("내용입력");
		String contents = sc.nextLine();

		boardVO.setPo_tit(title);
		boardVO.setPo_con(contents);
		boardVO.setPo_type(boardType);
		
		//포스트넘버 최댓값 구해서 ++해주기
		List<BoardVO> list = bDAO.selectAll();
		int a = 0;
		int max = 0;
		for (BoardVO po_no : list) {
			a = po_no.getPo_no();
			if (max < a) {
				max = a;
			}
		}
		boardVO.setPo_no(++max);
		
		
		
		//boardVO.setPo_no(bDAO.selectAll(boardType));

		// String id = LoginControl.getLoginInfo().getMem_id();
		// String nic = LoginControl.getLoginInfo().getMem_nick();

		// boardVO.setPo_id(id);
		// boardVO.setPo_nic(nic);

		bDAO.insert(anonPostInfo);
		bDAO.insert(boardVO);

	}

	//// 익명게시판3 목록출력
	private void selectAll() {
		List<BoardVO> list = bDAO.selectAll(boardType);
		System.out.println("\n목록");
		System.out.println("(글번호) \t (제목)");

		for (BoardVO boardVO : list) {
			System.out.printf("%d   \t %s\n", boardVO.getPo_no(), boardVO.getPo_tit());
		}
	}

	// 글삭제
	private void delete() {

		// 포스트넘버 입력
		System.out.println("삭제할 글 번호를 입력해 주세요");
		System.out.print("글번호>");
		int postNo = intScan();

		// 글 검색
		BoardVO findVO = bDAO.selectOne(postNo);

		if (findVO == null) {
			System.out.println("등록된 글이 없습니다.");
			return;
		}

		BoardVO boardVO = new BoardVO();
		boardVO.setPo_no(postNo);
		boardVO.setPo_type(boardType);
		boardVO.setPo_noId(AnonPostId.getPo_noId());
		boardVO.setPo_pw(AnonPostId.getPo_pw());

		// DB에서 삭제
		bDAO.anonDelete(boardVO);

	}

	// 댓글보여주기
	private void commentSelectAll(int poNo) {

		List<CommentVO> list = cDAO.selectAll(poNo);
		if (list.isEmpty()) {
			System.out.println("댓글이 없습니다.");
			return;
		}

		for (CommentVO commentVO : list) {
			System.out.printf("%d     %s,%s\t %s\n", commentVO.getCo_no(), commentVO.getCo_con());
		}
	}

	// 글보기
	private void viewPost() {
		while (true) {
			// 글번호입력 scanner
			System.out.print("글번호>");
			int poNo = intScan();

			// 입력숫자해당 글보기 제목 닉네임,아이디 내용 출력
			BoardVO boardVO = bDAO.selectOne(poNo);
			if (boardVO == null) {
				System.out.println("해당 글이 없습니다.");
				return;
			}
			System.out.println("제목 : " + boardVO.getPo_tit());
			System.out.println("내용\n" + boardVO.getPo_con());

			// 댓글구분선
			System.out.println("댓글**");
			System.out.println("(댓글번호)   \t (내용)");

			// 댓글보기
			commentSelectAll(poNo);

			// menu출력 method//댓글상세/글수정/글삭제/뒤로가기
			viewPostMenuPrint();

			// 메뉴 scanner
			int menuNo = intScan();

			if (menuNo == 1) {
				// 댓글상세
				new CommentManagement().run();
			} else if (menuNo == 2) {
				// 글수정
				postUpdate(poNo);
			} else if (menuNo == 3) {
				// 부여받은 글 삭제
				anonDelete(poNo);
			} else if (menuNo == 4) {
				// 뒤로가기
				exit();
				break;
			} else {
				// 입력오류
				showInputError();
			}
		}
	}

	// 글수정
	private void postUpdate(int poNo) {

		// 게시글 검색
		BoardVO findVO = bDAO.selectOne(poNo);
		// System.out.println(findVO.getPo_tit()+findVO.getPo_con());

		// 수정할 정보 입력
		BoardVO boardVO = inputUpdatePost(findVO);
		// System.out.println(boardVO.getPo_tit()+boardVO.getPo_con());

		// DB 수정
		bDAO.updatePost(boardVO);

	}

	// 수정-2
	private BoardVO inputUpdatePost(BoardVO boardVO) {

		System.out.print("제목 수정 (수정하지 않을 경우 0 입력) >");
		String title = sc.nextLine();
		if (!title.equals("0")) {
			boardVO.setPo_tit(title);
		}

		System.out.print("내용 수정 (수정하지 않을 경우 1 입력) >");
		String contents = sc.nextLine();
		if (!contents.equals("1")) {
			boardVO.setPo_con(contents);
		}

		return boardVO;

	}

	// 익명글보기 드가서 글삭제
	private void anonDelete(int poNo) {

		// 익명게시판 글쓰기 아이디 비밀번호
		System.out.print("익명아이디 입력>");
		String po_noId = sc.nextLine();
		System.out.print("익명비밀번호 입력>");
		String po_pw = sc.nextLine();

		BoardVO boardVO = new BoardVO();
		boardVO.setPo_no(poNo);
		boardVO.setPo_type(boardType);
		boardVO.setPo_noId(po_noId);
		boardVO.setPo_pw(po_pw);

		// DB에서 삭제
		if (bDAO.selectOne(poNo).getPo_noId().equals(po_noId) && bDAO.selectOne(poNo).getPo_pw().equals(po_pw)) {
			bDAO.anonDelete(boardVO);
		} else {
			System.out.println("아이디와 비밀번호를 다시 입력해주세요");
			return;
		}
	}

//	// 글보기 드가서 글삭제
//	private void delete(int poNo) {
//
//		BoardVO boardVO = new BoardVO();
//		boardVO.setPo_no(poNo);
//		boardVO.setPo_type(boardType);
//
//		// DB에서 삭제
//		bDAO.delete(boardVO);
//
//	}

	// 글보기 들어가면 나오는 menu출력 method//댓글상세/글수정/글삭제/뒤로가기
	private void viewPostMenuPrint() {
		System.out.println("==============================");
		System.out.println("1.댓글상세 2.글수정 3.글삭제 4.뒤로가기");
		System.out.println("==============================");
	}
}
